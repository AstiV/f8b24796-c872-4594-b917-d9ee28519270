import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ClubEvent } from './events';
import { Observable, Subscription, filter, map, of } from 'rxjs';
import { EventApiService } from './event-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'Event App';
  events$: Observable<ClubEvent[]>;
  cartEventList: ClubEvent[] = [];
  cartEventCount = 0;
  subscriptions = new Subscription();
  shoppingCartIsVisible = false;
  resultEvents: { date: Date; events: ClubEvent[] }[] = [];
  resultEventsObservable$: Observable<{ date: Date; events: ClubEvent[] }[]>;
  eventSearchTerm: string = '';

  constructor(
    private eventApi: EventApiService,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.events$ = this.eventApi.getAll();

    this.subscriptions.add(
      this.events$.subscribe((events) => {
        let set = new Set(events.map((item) => item.date));
        set.forEach((date) => {
          this.resultEvents.push({
            date: date,
            events: events.filter((item) => item.date === date),
          });
        });
        this.resultEvents.forEach((result) =>
          result.events.sort(
            (a: ClubEvent, b: ClubEvent) =>
              new Date(a.startTime).getTime() - new Date(b.startTime).getTime()
          )
        );
        this.resultEventsObservable$ = of(this.resultEvents);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  toggleSoppingCartVisibility() {
    this.shoppingCartIsVisible = !this.shoppingCartIsVisible;
  }

  addEventToCart(event: ClubEvent) {
    this.cartEventList.push({ ...event });
    this.cartEventCount += 1;

    this.resultEvents.forEach((resultItem) => {
      const filteredEvents = resultItem.events.filter(
        (item) => item._id !== event._id
      );
      resultItem.events = filteredEvents;
    });
  }

  removeEventFromCart(event: ClubEvent) {
    this.cartEventCount -= 1;
    const filteredCartEventList = this.cartEventList.filter(
      (item) => item._id !== event._id
    );
    this.cartEventList = filteredCartEventList;

    this.resultEvents.forEach((resultItem) => {
      if (resultItem.date === event.date) {
        resultItem.events.push({ ...event });
        resultItem.events.sort(
          (a: ClubEvent, b: ClubEvent) =>
            new Date(a.startTime).getTime() - new Date(b.startTime).getTime()
        );
      }
    });
    this.changeDetectorRef.detectChanges();
  }

  updateEventSearchTerm(input: Event) {
    this.eventSearchTerm = (input.target as HTMLInputElement).value;
    console.log('this.eventSearchTerm', this.eventSearchTerm);
  }
}
