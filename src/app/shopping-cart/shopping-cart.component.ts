import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ClubEvent } from '../events';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss'],
})
export class ShoppingCartComponent {
  @Input() events: ClubEvent[] = [];
  @Input() isOpen = false;
  @Output() eventRemoved = new EventEmitter();

  removeEventFromCart(event: ClubEvent) {
    this.eventRemoved.emit(event);
  }
}
