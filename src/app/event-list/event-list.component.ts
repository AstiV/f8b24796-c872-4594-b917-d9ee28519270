import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ClubEvent } from '../events';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss'],
})
export class EventListComponent {
  @Input() events: ClubEvent[] | null = [];
  @Input() eventSearchTerm: string = '';
  @Output() eventAdded = new EventEmitter();

  addEventToCart(event: ClubEvent) {
    this.eventAdded.emit(event);
  }
}
