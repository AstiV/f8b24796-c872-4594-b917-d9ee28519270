import { Pipe, PipeTransform } from '@angular/core';
import { ClubEvent } from './events';

@Pipe({
  name: 'eventFilter',
})
export class EventFilterPipe implements PipeTransform {
  transform(
    events: ClubEvent[] | null,
    searchTerm: string | null
  ): ClubEvent[] {
    if (!searchTerm) {
      return events || [];
    }

    if (!events) {
      return [];
    }

    return events.filter((event) =>
      event.title.toLowerCase().includes(searchTerm.toLowerCase())
    );
  }
}
