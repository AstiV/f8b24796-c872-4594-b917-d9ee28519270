import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ClubEvent } from '../events';

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.scss'],
})
export class EventCardComponent {
  @Input() event: ClubEvent;
  @Input() addButton: boolean;
  @Output() eventAdded = new EventEmitter();
  @Output() eventRemoved = new EventEmitter();

  addEventToCart(event: ClubEvent) {
    this.eventAdded.emit(event);
  }

  removeEventFromCart(event: ClubEvent) {
    this.eventRemoved.emit(event);
  }
}
