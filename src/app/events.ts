export interface ClubEvent {
  _id: string;
  title: string;
  flyerFront: string;
  date: Date;
  startTime: Date;
  endTime: Date;
  venue: Venue;
  private: boolean;
}

export interface Venue {
  _id: string;
  name: string;
  direction: string;
}
