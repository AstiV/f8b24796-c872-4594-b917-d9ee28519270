import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ClubEvent } from './events';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EventApiService {
  constructor(private http: HttpClient, private router: Router) {}

  getAll() {
    return this.http
      .get<ClubEvent[]>(
        'https://teclead-ventures.github.io/data/london-events.json'
      )
      .pipe(
        map((events) =>
          events.sort(
            (a: ClubEvent, b: ClubEvent) =>
              new Date(a.date).getTime() - new Date(b.date).getTime()
          )
        )
      );
  }
}
